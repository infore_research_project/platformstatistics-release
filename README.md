# Metric collection tools
Tools and utilities for collecting metrics from resource managers and platforms.

## Build with maven

	mvn clean package

## REST API
This project uses the REST API of Apache Spark, Apache Flink and Apache YARN to poll statistics
and other useful information regarding running jobs, cluster load and available slots. Although, 
this project was developed for earlier versions of Flink (1.5 and earlier) and Spark it should , however, 
still work for certain metrics and serve as an example for future projects.

<h4>Check the tests folder for an example.</h4>

## Logstash
The elastic stack can also be used to gather and process metrics from BigData platforms.
An easy and reliable way to tap into these metrics is via JMX connections. In the past,
the Logstash component of the elastic stack has been used to probe resource managers and
periodically collect statistics like cluster load and job status. Im attaching the 
Logstash configuration and Flink monitoring files.


## Contact
George Stamatakis
For any questions feel free to contact me at giorgoshstam@gmail.com
