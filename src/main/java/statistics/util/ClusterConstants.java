package statistics.util;

public class ClusterConstants {
    public static final String flink_master = "localhost:8081";
    public static final String spark_master = "localhost:7077";
    public static final String yarn_master = "localhost:8189";
}
